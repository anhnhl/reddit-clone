import { User } from './../entities/User';
import { Arg, Ctx, Field, InputType, Mutation, ObjectType, Resolver } from 'type-graphql';
import argon2 from 'argon2';
import { MyContext } from 'src/types';

@InputType()
class UsernamePasswordInput {
    @Field()
    username: string;
    @Field()
    password: string;
}

@ObjectType()
class FieldError {
    @Field()
    field: string;

    @Field()
    message: string;
}

@ObjectType()
class UserResponse {
    @Field(() => [FieldError], { nullable: true })
    errors?: FieldError[];

    @Field(() => User, { nullable: true })
    user?: User;
}

@Resolver()
export class UserResolver {
    @Mutation(() => UserResponse)
    async register(@Arg('options') options: UsernamePasswordInput): Promise<UserResponse> {
        const hashedPassword = await argon2.hash(options.password);
        const user = User.create({ username: options.username, password: hashedPassword });
        try {
            await user.save();
        } catch (error) {
            if (error?.errno === 1062 || error?.sqlMessage.includes('Duplicate entry')) {
                return {
                    errors: [
                        {
                            field: 'username',
                            message: 'Username has been taken',
                        },
                    ],
                };
            }
        }
        return { user };
    }

    @Mutation(() => UserResponse)
    async login(
        @Arg('options') options: UsernamePasswordInput,
        @Ctx() { req }: MyContext
    ): Promise<UserResponse> {
        const user = await User.findOne({ username: options.username });

        if (!user) {
            return {
                errors: [
                    {
                        field: 'username',
                        message: "User doesn't exists",
                    },
                ],
            };
        }

        const valid = await argon2.verify(user.password, options.password);
        if (!valid) {
            return {
                errors: [{ field: 'password', message: 'Incorrect password' }],
            };
        }

        req.session!.userId = user.id;

        return { user };
    }
}
