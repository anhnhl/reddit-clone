import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { Post } from './../entities/Post';

@Resolver()
export class PostResolver {
    @Query(() => [Post])
    posts(): Promise<Post[]> {
        return Post.find();
    }

    @Query(() => Post, { nullable: true })
    post(@Arg('id') id: number): Promise<Post | undefined> {
        return Post.findOne(id);
    }

    @Mutation(() => Post)
    async createPost(@Arg('title') title: string): Promise<Post> {
        const newPost = Post.create({ title });
        await newPost.save();
        return newPost;
    }

    @Mutation(() => Post, { nullable: true })
    async updatePost(
        @Arg('id') id: number,
        @Arg('title', () => String, { nullable: true }) title: string
    ): Promise<Post | null> {
        const post = await Post.findOne({ where: { id } });
        if (!post) {
            return null;
        }
        if (typeof title !== 'undefined') {
            post.title = title;
        }
        await post.save();
        return post;
    }

    @Mutation(() => Boolean)
    async deletePost(@Arg('id') id: number): Promise<boolean> {
        const post = await Post.findOne({ where: { id } });
        if (!post) return false;
        await post.remove();
        return true;
    }
}
