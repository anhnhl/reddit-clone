import {MigrationInterface, QueryRunner} from "typeorm";

export class genratePost1617548689834 implements MigrationInterface {
    name = 'genratePost1617548689834'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `post` (`id` int NOT NULL AUTO_INCREMENT, `created_at` date NOT NULL, `updated_at` date NOT NULL, `title` text NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `post`");
    }

}
