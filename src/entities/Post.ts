import { Field, ID, ObjectType } from 'type-graphql';
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Post extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field(() => String)
    @Column('date')
    created_at = new Date();

    // TODO: Add on update hook: new Date()
    @Field(() => String)
    @Column('date')
    updated_at = new Date();

    @Field()
    @Column('text')
    title: string;
}
