module.exports = {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'luonganh',
    database: 'reddit-clone',
    synchronize: false,
    entities: ['./entities/*.ts', 'dist/entities/*.js'],
    migrations: ['./migration/*.ts', 'dist/migration/*.js'],
    subscribers: ['./subscribers/*.ts', 'dist/subscribers/*.js'],
    cli: {
        entitiesDir: 'src/entities',
        migrationsDir: 'src/migration',
        subscribersDir: 'src/subscriber',
    },
};
